local fennel = require("lib/fennel")
debug.traceback = fennel.traceback
table.insert(package.loaders or package.searchers, fennel.make_searcher({correlate=true}))
require("game")