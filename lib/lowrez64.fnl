(local lowrez64 {:mouse {}})
(fn lowrez64.setup [x y overscan]
  (set lowrez64.overscan (or overscan false))
  (set lowrez64.sizeX (or x 64))
  (set lowrez64.sizeY (or y lowrez64.sizeX))
  (if (< x (or y 0))
      (set lowrez64.scaler (/ (love.graphics.getHeight) lowrez64.sizeY))
      (set lowrez64.scaler (/ (love.graphics.getWidth) lowrez64.sizeX)))
  (set lowrez64.x (- (/ (love.graphics.getWidth) 2)
                   (* lowrez64.scaler (/ lowrez64.sizeX 2))))
  (set lowrez64.y (- (/ (love.graphics.getHeight) 2)
                   (* lowrez64.scaler (/ lowrez64.sizeY 2))))
  (set lowrez64.canvas (love.graphics.newCanvas lowrez64.sizeX lowrez64.sizeY))
  (lowrez64.canvas:setFilter :nearest)
  (lowrez64.resize (love.graphics.getDimensions)))
(fn lowrez64.start []
  (love.graphics.setCanvas lowrez64.canvas)
  (love.graphics.clear))
(fn lowrez64.finish []
  (love.graphics.setCanvas)
  (love.graphics.draw lowrez64.canvas lowrez64.x lowrez64.y 0 lowrez64.scaler))
(fn lowrez64.resize [w h]
  (if lowrez64.overscan
      (do
        (set lowrez64.scaler (/ h lowrez64.sizeY))
        (when (< (/ w lowrez64.scaler) (/ lowrez64.sizeX 2))
          (set lowrez64.scaler (* (/ w lowrez64.sizeX) 2)))
        (set lowrez64.x (/ (- w (* lowrez64.scaler lowrez64.sizeX)) 2))
        (if (> (/ w lowrez64.scaler) lowrez64.sizeX)
            (do
              (set lowrez64.right lowrez64.sizeX)
              (set lowrez64.left 0))
            (do
              (set lowrez64.right (/ (- w lowrez64.x) lowrez64.scaler))
              (set lowrez64.left (- (/ lowrez64.x lowrez64.scaler))))))
      (do
        (if (< (/ h lowrez64.sizeY) (/ w lowrez64.sizeX))
            (set lowrez64.scaler (/ h lowrez64.sizeY))
            (set lowrez64.scaler (/ w lowrez64.sizeX)))
        (set lowrez64.right lowrez64.sizeX)
        (set lowrez64.left 0)
        (set lowrez64.x (- (/ w 2) (* lowrez64.scaler (/ lowrez64.sizeX 2))))))
  (set lowrez64.y (- (/ h 2) (* lowrez64.scaler (/ lowrez64.sizeY 2)))))
(fn lowrez64.mouse.getPosition []
  (let [mx (math.floor (* (/ (- (love.mouse.getX) lowrez64.x)
                             (* lowrez64.scaler lowrez64.sizeY))
                          lowrez64.sizeY))
        my (math.floor (* (/ (- (love.mouse.getY) lowrez64.y)
                             (* lowrez64.scaler lowrez64.sizeX))
                          lowrez64.sizeX))]
    (values mx my)))
(fn lowrez64.mouse.getX []
  (math.floor (* (/ (- (love.mouse.getX) lowrez64.x)
                    (* lowrez64.scaler lowrez64.sizeX))
                 lowrez64.sizeX)))
(fn lowrez64.mouse.getY []
  (math.floor (* (/ (- (love.mouse.getY) lowrez64.y)
                    (* lowrez64.scaler lowrez64.sizeY))
                 lowrez64.sizeY)))
(fn lowrez64.newImage [source]
  (let [image (love.graphics.newImage source)]
    (image:setFilter :nearest)
    image))
(fn lowrez64.newTileSet [image x y]
  (let [quad {: x : y}]
    (for [i 0 (- (* (/ (image:getWidth) x) (/ (image:getHeight) y)) 1) 1]
      (tset quad i
            (love.graphics.newQuad (* (% i (/ (image:getWidth) x)) x)
                                   (* (math.floor (/ i (/ (image:getWidth) x)))
                                      y) x y
                                   (image:getDimensions))))
    quad))
lowrez64