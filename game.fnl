;; global parameters and constants

(local lowrez64 (require "lib/lowrez64"))

;; window properties & initialization
(local CONF {:title "Minimal LÖVE"
             :identity :minimal-love
             :width 640
             :height 640
             :flags {:resizable true
                     :vsync false
                     :minwidth 256
                     :minheight 256}
             :scalex nil
             :scaley nil})

(local gfx love.graphics)
;(local glyphs (.. " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRST"
;               "UVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"))
;(local font-mono (love.graphics.newImageFont "fonts/font-mono.png" glyphs))

(local glyphs " +-/='_(),.:?[]abcdefghijklmnopqrstuvwxyz0123456789")
(local font-3x3 (love.graphics.newImageFont "assets/fnt/3x3.png" glyphs 1))


(local debug (gfx.newText font-3x3 ""))
(local hello (gfx.newText font-3x3 ""))
(hello:set "hello world")

(fn love.load []
  (love.window.setTitle CONF.title)
  (love.filesystem.setIdentity CONF.identity)
  (love.window.setMode CONF.width CONF.height CONF.flags)
  (gfx.setDefaultFilter :nearest :nearest 0)
  (love.graphics.setFont font-3x3)	
  
  (lowrez64.setup 64))

(fn love.draw [] 
  (lowrez64.start)

  (gfx.clear 0.5 0 0 )

  (gfx.setColor 255 255 255)
  (gfx.draw debug 0 0)
  (gfx.draw hello 0 61)

  (lowrez64.finish))

(fn love.resize [w h]
  (lowrez64.resize w h))

(fn handle-keyboard-input [] 
  (when (love.keyboard.isDown :escape)
    (love.event.quit)))

(fn love.update [dt]
  (local fps (love.timer.getFPS))
  (debug:set (.. fps " fps"))
  (handle-keyboard-input))

