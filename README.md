# Minimal LÖVE
Minimal LÖVE is the absolute functional minimum required to use Fennel in LÖVE2D and this is also a LowRezJam starter kit

What does the template provide:
 * Maintain/force 64x64 pixel perfect canvas with aspect ratio resizing
 * 3x3 minimal glyph font

## Attributions
 * Fennel https://fennel-lang.org/
 * LÖVE2D https://love2d.org/
 * Lowrez Jam Starter Kit for Love2D https://github.com/tcfunk/lowrezjam-starterkit-love2d
 * maid64 https://github.com/adekto/maid64

